<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
     <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    
    <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<c:set var="data" value="singapore,Tokyo,Mumbai,Lucknow"></c:set>
	
	<h3>Split demo</h3> 
	${data} <br>
	<c:set var="cityArray" value="${fn:split(data,',')}" />
	<c:forEach var="tempCity" items= "${ cityArray}">
		${tempCity}<br>
	</c:forEach> 
	
	<h3>Join demo</h3>
	<c:set var="fun"  value="${fn:join(cityArray,',')}"/>
	Result of joining: ${fun}
</body>
</html>