<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    
    <%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
    
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<c:set var="data" value= "pranjal"/>
	
	length of the string <b>${data}</b>: ${fn:length(data)}
	
	<br> <br>
	
	Uppercase version of string <b>${data}</b>: ${fn:toUpperCase(data)}
	
	<br> <br>
	
	does the string <b>${data}</b> starts with : ${fn:startsWith(data,"luv")}
	
	
</body>
</html>